import socket
import random
import Blum

if __name__ == "__main__":
    s = socket.socket()
    port = 12345
    s.bind(('', port))
    s.listen(5)

    m = '10011100000100001100'
    print('Plaintext: %s' %(m))
    x0 = 159201
    print('Listening...')
    while(True):
        c, addr = s.accept()
        N = int(c.recv(1024))
        ciphertext, xArrMax  = Blum.encrypt(N, m, x0)
        print('Ciphertext: %s xL: %s' %(ciphertext, xArrMax))
        c.send(ciphertext)
        c.send(str(xArrMax))

        c.close()
        break