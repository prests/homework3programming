import random
import math

def BBS(x, N): #Blum Blum Shum Pseudo Random Number Generator
    x1 = (x**2) % N
    x1Bin = str(bin(x1))[2:]
    return x1, x1Bin[len(x1Bin)-1]

def decrypt(ciphertext, y, p, q, N, a, b):
    m = ''

    #setup to find initial seed
    rPExp = (((p+1)/4)**(len(ciphertext)+1))%(p-1)
    rQExp = (((q+1)/4)**(len(ciphertext)+1))%(q-1)
    rP = (y**rPExp)%p
    rQ = (y**rQExp)%q

    xArr = []
    keyStream = ''
    
    x = (rQ*a*p + rP*b*q) % N #calculate initial seed
    print('x0: %s' %(x))
    x0Bin = str(bin(x))[2:]
    xArr.append(x)
    keyStream = keyStream + str(x0Bin[len(x0Bin)-1])

    for i in range(0,len(ciphertext)-1):
        x, leastSigBit = BBS(x, N)
        xArr.append(x)
        keyStream = keyStream + str(leastSigBit) #create key stream

    for i in range(0,len(ciphertext)):  
        m = m + str(int(keyStream[i]) ^ int(ciphertext[i])) #XOR ciphertext with keystream

    return m, xArr[len(xArr)-1] #return plaintext

def encrypt(N, m, x0):
    cipher = ''

    x0Bin = str(bin(x0))[2:]
    leastSigBit = x0Bin[len(x0Bin)-1] #get least sig bit of seed

    xArr = []
    keyStream = ''
    x = x0
    for i in range(0,len(m)+1):
        keyStream = keyStream + leastSigBit #add least sig bit to key stream
        x, leastSigBit = BBS(x, N) #get least sig bit and get X
        xArr.append(x)

    for i in range(0,len(m)):
        cipher = cipher + str(int(keyStream[i]) ^ int(m[i])) #XOR msg with keystream

    return cipher, xArr[len(xArr)-1] #return cipher and xL
    
