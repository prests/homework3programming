import socket
import Blum

if __name__ == '__main__':
    p = 499
    q = 547
    a = -57
    b = 52
    N = p*q #public key
    s = socket.socket()
    host = '127.0.0.1'
    port = 12345

    s.connect((host, port))
    while(True):
        s.send(str(N))

        ciphertext = s.recv(1024)
        nMax = int(s.recv(1024))
        m, xArrMax = Blum.decrypt(ciphertext, nMax, p, q, N, a, b)
        print('Decrypted: %s' %(m))
        if(m == '10011100000100001100'):
            print('Decrypted message equals plaintext!')
        s.close()
        break