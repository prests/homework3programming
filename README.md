# Homework3programming

Network Security Blum-Goldwasser cryptosystem

This demo uses a client-server relationship, where Bob is the server and Alice is the client. The way it works is as follows:

1. Alice sends bob the public key N = p*q
2. Bob generates a keystream with x0 which was given. Bob uses Blum Blum Shum's pseudo random key generator to calculate x_i = (x_i-1)^2 mod N
3. Bob XOR's the message he wants to send with the keystream which is the least significate bit of every x value calculated
4. Bob sends the ciphertext and and the last X value calculated to Alice
5. Alice uses the max x value p,q,a,b (which are given) to recalculate the x0 seed and repeats the blum blum shum process to find all the x values and least significant bits
6. Alice XOR's the ciphertext with the keystream she generated and gets the message that Bob intended to send